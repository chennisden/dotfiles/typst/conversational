#import "@preview/ctheorems:1.1.3": *

#let LightGray = rgb("#e0e0e0")
#let Black = rgb("#000000")

#let tfmt = strong
#let nfmt = it => strong[(#it)]

#let theo = thmbox(
	"global",
	"Theorem",
	namefmt: nfmt,
	fill: LightGray,
	radius: 0em,
	stroke: (left: Black),
	base_level: 1
)

#let defi = thmbox(
	"global",
	"Definition",
	namefmt: nfmt,
	fill: LightGray,
	radius: 0em,
	stroke: (left: Black),
	base_level: 1
)

#let exam = thmplain(
	"global",
	"Example",
	titlefmt: tfmt,
	namefmt: nfmt,
	base_level: 1
)

#let exer = thmplain(
	"global",
	"Exercise",
	titlefmt: tfmt,
	namefmt: nfmt,
	base_level: 1
)

#let pro = thmproof("proof", "Proof")
#let sol = thmproof("solution", "Solution")

// Table of contents
#let toc = {
	pagebreak(weak: true)
	show outline.entry.where(
		level: 1
	): it => {
		v(1em, weak: true)
		strong(it)
	}
	outline(
		title: "Table of Contents"
	)
}

// Global show rule
#let setup(title: none, author: none, book: false, footer: true, body) = {
	// Sections
	set heading(numbering: "1.1")
	show heading: it => {
		if (book and it.depth == 1) {
			pagebreak(weak: true)

			if (it.numbering != none) {
				set text(size: 2em)
				"Chapter "
				counter(heading).display()
				v(0em)
			}
			set text(size: 1.5em)
			it.body
			v(0.5em)
		} else {
			if (it.numbering != none) {
				counter(heading).display()
				h(1em)
			}
			it.body
			v(0.5em)
		}
		if (it.depth == 1) {
			counter(footnote).update(0)
		}
	}

	// Links
	show link: underline
	show ref: underline

	// List/enum
	set list(indent: 1em)
	set enum(indent: 1em)

	show: thmrules.with(qed-symbol: $square$)

	if (title != none) {
		set align(center)
		text(size: 2em, weight: "bold", title)
	}
	if (author != none) {
		set align(center)
		text(size: 1.5em, author)
	}
	if (title != none or author != none) {
		set align(center)
		text(size: 1.25em, datetime.today().display("[month repr:long] [day], [year]"))
		v(4em)
	}

	// Page setup. Do it before including body content
	// because it will take effect then and
	// because changing page setup forces a pagebreak:
	// https://typst.app/docs/guides/page-setup-guide/
	//
	// Includes footers, which have page numbers
	set page(footer: context {
		set text(size: 12pt, weight: "regular")
		if calc.even(counter(page).get().at(0)) {
			let elems = query(
				selector(heading).before(here()),
			)
			counter(page).display("1")
			if (elems.len() > 0 and elems.last().level >= 2) {
				h(1fr)
				strong("Section ")
				strong(counter(heading).display())
				h(1em)
				strong(elems.last().body)
			}
		} else {
			let elems = if book {
					query(
						selector(heading.where(level: 1)).before(here())
					)
				} else {
					query(
						selector(heading).before(here()),
					)
				}
			if elems.len() > 0 {
				if book {
					let chapter_counter = counter(heading.where(level: 1))
					if (chapter_counter.get().at(0) > 0) {
						strong("Chapter ")
						strong(chapter_counter.display())
					}
				} else {
					let section_counter = counter(heading)
					if (section_counter.get().at(0) > 0) {
						strong("Section ")
						strong(section_counter.display())
					}
				}
				h(1em)
				strong(elems.last().body)
				h(1fr)
			}
			counter(page).display("1")
		}
	}) if footer

	body
}
